import os
import gitlab

gl = gitlab.Gitlab('https://gitlab.com/', private_token=os.getenv('GITLAB_PRIVATE_TOKEN'))

# Get a project by ID
# project_id = 38554962
# project = gl.projects.get(project_id)
# print(project)

# Get a project by name with namespace
# project_name_with_namespace = "garzon.development/simple-gitlab-api"
# project = gl.projects.get(project_name_with_namespace)
# attrs = project.customattributes.list()
# print(attrs)

# List all the projects
projects = gl.projects.list(get_all=False)
print(projects)
for project in projects:
    print(project)

# # Given the current project
# project_id = os.getenv('CI_PROJECT_ID')
# print(">>>",project_id)
# project = gl.projects.get(project_id)

# print("-----------------")
# for pipe in project.pipelines.list():
#     print(pipe)

# print("-----------------")
# for job in project.jobs.list():
#     print(job)
#     print()